**TLDR;** Please preview the slides at [https://rwth-crmasters-sose22.gitlab.io/templates/slide-template](https://rwth-crmasters-sose22.gitlab.io/templates/slide-template)

Please change the link above when you fork/clone the repo to reflect the URL found in [this section](#the-link-to-view-your-presentation)

# Slide Deck Template for DDP

## How to use

Please fork this repo and create a new `.md` file named `index.md`.

Using the template in `template.md`, use the features provided to create your own presentation.

If you do everything correctly, then you will have a URL that you can share with other people. Namely, you will never have to carry a flash disk around.

## Testing in vscode

Using the [Marp for VS Code](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode) plugin.

Note that to preview the markdown presentation you need to press the Marp icon in the editor and choose **Toggle Marp Feature for Current Markdown**.

## Better preview in the browser

Unfortunately, the plugin won't render many of the extra features in this template. For that, you will need two things:
1. Install the [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) plugin for VS Code
2. Download the [Marp CLI](https://github.com/marp-team/marp-cli/releases/latest) for your operating system and place it in the repository folder

### Command needed to preview the markdown 

After placing the executable of the _Marp CLI_ in the same directory, you need to use the shell to navigate to that directory and use the executable to preview the markdown.

You can open a terminal from within of Vscode from the terminal menu.

On Windows, the command needed in PowerShell to preview your presentation is the following:

```
.\marp.exe --engine .\marp.config.js --watch .\index.md
```

After running this command, a file named `index.html` will be created and automatically updated when you change the source `md` file. To view the presentation in the browser, right click on the `index.html` file and choose **Open with Live Server**.

## Plugins and features

This template supports the following plugins:
- Emoji textual representations
- Highlighted text using `==text==`
- **plantuml** diagrams between `@startuml` and `@enduml`
- To fix distortion of **plantuml** diagrams we used the `:::` codeblock-like containers with a CSS class called `fence`
- CSS classes using `{.css-class}` after a markdown segment, note that you have to use CSS to style that class (in the `<style>...</style>` part of the presentation `md` file)
- HTML is also allowed
- Check Marpit documentation for backgrounds, themes, and more [here](https://marpit.marp.app/markdown)

### The link to view your presentation

If the pipelines succeeded after your last push, then go to _Settings -> Pages_ and there you will find the link to your presentation.

If you named your source file `index.md`, then the presentation should be available at that link.
