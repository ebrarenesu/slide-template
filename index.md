---
title: Real Time Translation in Remote AR Assistance
description: Making Remote AR Assistance more effective by using AI in simultaneous translation
author: Beyza Irmak, Ebrar Enes Ucmaz
keywords: marp,marp-cli,slide,AR
url: 
marp: true
image: 
---

# Remote Assistance and Collobration with **AR**
![](assets/remote_assist.jpg)


---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Our Approach

- According to our previous experience, one of the most serious problems in the field of construction was the inability to effectively manage time. Communication difficulties between all experts and employees made it more complicated to solve the problems at the construction site as soon as possible.
- It is aimed to use AR technology in the field of construction as Remote Assistance, from experts to workers, when necessary, and to solve the problems as soon as possible. In addition, with the increase in the frequency and ease of communication, a more supervised and controlled construction process is aimed. Accessing experts from any location and allowing them to see through the on-site worker will allow both to work hands-free and to solve problems with collaboration. This form of communication, which is recommended instead of classical communication tools, is safe and systematic, allowing time to be managed efficiently and ensuring a smooth flow of information. It allows for sharing files, 3d holograms, models, animations, and capturing images.
- Our approach is to provide a solution to the language barrier problem, which is one of the human-induced problems that may arise when using Remote AR Assistance to increase communication efficiency. Simultaneous translation with AI technology makes the remote assistance process more understandable and efficient.

---

# Mindmap

::: fence
@startuml
@startmindmap
+[#Orange] AR in Construction
++[#lightblue] Remote assistance and collobration
+++[#lightblue] Communication efficiency
++++ Site conditions
+++++ Various accesibility issues
++++++ Distance
++++++ Physical barriers
+++++ Operational issues
++++++ Different work intensities at the construction site
++++++ Have a deep hierarchy
++++[#lightblue] Human-related issues
+++++[#lightblue] Language barrier
++++++[#lightblue] Unclear communication
++++++[#lightblue] Difficulties in the flow of information
+++++ Timing
++++++ Frequency of experts on site
++++++ Shift work issues
+++++ Multitasking issues
++++++ Insufficient number of experts
++++++ Increase in the liabilities of experts
@endmindmap
@enduml
:::
---


# Inspirations
- [Google Teases AR Glasses With Live Translate](https://www.youtube.com/watch?v=8sMIbn71_WY&ab_channel=UploadVR)
- [AKULAR](https://www.youtube.com/watch?v=sN4l4Y8IbYY&ab_channel=AKULAR)
- [TeamViewer Assist AR & Schuler](https://www.youtube.com/watch?v=Z5DVsF1EVS8&ab_channel=TeamViewer)
- [LibreStream Remote Expert Assistance](https://librestream.com/platform/remote-expert/?utm_source=google&utm_medium=cpc&utm_campaign=MS+GLP-B+Remote+Expert&creative=578558384792&keyword=remote%20ar%20assistance&matchtype=b&network=g&device=c&gclid=CjwKCAjwj42UBhAAEiwACIhADgjn2A8JdWZCQTB4fEPmB4FNP1dhcyqWxl0B6SKzBffUJAWQyuUOrxoC3OMQAvD_BwE)
- [Vidscribe](https://vidscribe.in)
- [Stenomatic](https://www.stenomatic.ai/)
# Open Source Projects
- [AR Remote Support called AgoralO](https://github.com/AgoraIO-Community/AR-Remote-Support)
- [Mixed Reality Remote Support called RemoteSpark](https://github.com/kspark-scott/MixedReality-WebRTC)
- [AR Remote Support called SightCall](https://github.com/sightcall)
- [Real Time Translator called RTranslator](https://github.com/niedev/RTranslator)
- [Real Time Translator called Interactio](https://github.com/MaxNeedsSnacks/Interactio)







---

# Thank you for listening!

